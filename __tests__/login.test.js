// __tests__/login.test.js

import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import Login from '../pages/login';

// Mock useRouter
jest.mock('next/router', () => ({
  useRouter: () => ({
    push: jest.fn(),
  }),
}));

// Mock fetch
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ token: 'mocked-token' }),
    ok: true,
  })
);

describe('Login Component', () => {
  test('renders login form', () => {
    render(<Login />);
    expect(screen.getByText('Connexion')).toBeInTheDocument();
    expect(screen.getByLabelText('Email ou Numéro:')).toBeInTheDocument();
    expect(screen.getByLabelText('Mot de passe:')).toBeInTheDocument();
    expect(screen.getByText('Se connecter')).toBeInTheDocument();
  });

  test('submits login form successfully', async () => {
    render(<Login />);
    fireEvent.change(screen.getByLabelText('Email ou Numéro:'), {
      target: { value: 'test@example.com' },
    });
    fireEvent.change(screen.getByLabelText('Mot de passe:'), {
      target: { value: 'password123' },
    });
    fireEvent.click(screen.getByText('Se connecter'));

    // Wait for the asynchronous operations
    await waitFor(() => {
      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith('/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: 'test@example.com',
          password: 'password123',
          action: 'login',
        }),
      });
      expect(localStorage.setItem).toHaveBeenCalledWith('token', 'mocked-token');
      expect(screen.getByText('Received Token: mocked-token')).toBeInTheDocument();
    });
  });
  
  // Add more test cases as needed
});
