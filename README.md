# Authentification et Gestion de Profil avec Partie Admin

## Introduction
Cette section de l'application gère l'authentification et la gestion des profils des utilisateurs, incluant une fonctionnalité d'administration. Les contributeurs à cette partie sont Ghassen HAOUALA et Souheil DABBABI.

## Configuration
Les identifiants de l'administrateur (nom d'utilisateur et mot de passe) sont stockés dans le fichier `.env.local`.

## Page d'Accueil
La page par défaut, ou la page d'accueil, est définie comme la page d'inscription pour les utilisateurs.

## Accès à la Page d'Administration
Pour accéder à la page d'administration, vous devez ajouter `/admin` à l'URL de l'application.

### Exemple d'URL pour la Page d'Administration
```
https://votre-application.com/admin
```

## Remarque
Le principe de Reinitialisation de mot de passe est d'envoyer un nouveau mot de passe aleatoire au utilisateur par mail

## Captures d'écran
![Capture d'écran de la page d'inscription](Captures/Inscription.png)
![Capture d'écran de la page de connexion](Captures/Connexion.png)
![Capture d'écran de la page de Gestion de profil](Captures/Gestiondeprofil.png)
![Capture d'écran de la Reinitialisation de mot de passe](Captures/Reinitialisationdemotdepasse.png)
![Capture d'écran de la page Gestion d'approbation](Captures/Gestionapprobation.png)
![Capture d'écran de la page Gestion d'utilisateurs](Captures/Gestionutilisateurs.png)
![Capture d'écran de la base de données](Captures/BD.png)




