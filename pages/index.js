// pages/signup.js
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

const Signup = () => {
  const router = useRouter();
  const [formData, setFormData] = useState({
    nom: '',
    prenom: '',
    email: '',
    geolocalisation: '',
    statutManager: false,
    password: '',
    numero: '',
  });
  const [errorMessage, setErrorMessage] = useState('');

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    // Validation côté client
    if (!/^[0-9]+$/.test(formData.numero)) {
      setErrorMessage('Le numéro doit contenir uniquement des chiffres.');
      return;
    }
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(formData.email)) {
      setErrorMessage('Entrer un email valide');
      return;
    }
    if (formData.password.length < 8) {
      setErrorMessage('Le mot de passe doit contenir au moins 8 caractères.');
      return;
    }

    // Vérifier d'autres champs obligatoires ici si nécessaire

    try {
      const response = await fetch('/api/signup', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        router.push('/login');
      } else {
        const errorData = await response.json();
        setErrorMessage(errorData.message);
      }
    } catch (error) {
      console.error('Unexpected Error during Signup:', error);
    }
  };

  return (
    <div className="signup-container">
      <h2>Inscription</h2>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      <form onSubmit={handleSubmit}>
      <label>
          Email:
          <input
            type="text"
            name="email"
            value={formData.email}
            onChange={handleInputChange}
            required // Champ obligatoire
          />
        </label>
        <label>
          Numéro:
          <input
            type="text"
            name="numero"
            value={formData.numero}
            onChange={handleInputChange}
            required // Champ obligatoire
          />
        </label>
        <br />

        <label>
          Nom:
          <input
            type="text"
            name="nom"
            value={formData.nom}
            onChange={handleInputChange}
            required // Champ obligatoire
          />
        </label>
        <br />

        <label>
          Prénom:
          <input
            type="text"
            name="prenom"
            value={formData.prenom}
            onChange={handleInputChange}
            required // Champ obligatoire
          />
        </label>
        <br />

        <label>
          Géolocalisation:
          <input
            type="text"
            name="geolocalisation"
            value={formData.geolocalisation}
            onChange={handleInputChange}
            required // Champ obligatoire
          />
        </label>
        <br />

        <label>
          Statut de Manager:
          <input
            type="checkbox"
            name="statutManager"
            checked={formData.statutManager}
            onChange={() =>
              setFormData({
                ...formData,
                statutManager: !formData.statutManager,
              })
            }
          />
        </label>
        <br />

        <label>
          Mot de passe:
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
            required // Champ obligatoire
          />
        </label>
        <br />

        <button type="submit">S'inscrire</button>
      </form>
      <p>
        Vous avez un compte ?{' '}
        <Link href="/login">Connectez-vous ici</Link>
      </p>

      <style jsx>{`
        .signup-container {
          max-width: 400px;
          margin: auto;
          padding: 20px;
          border: 1px solid #ccc;
          border-radius: 8px;
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h2 {
          text-align: center;
          color: #333;
        }

        form {
          display: flex;
          flex-direction: column;
        }

        label {
          margin-bottom: 10px;
        }

        input {
          padding: 8px;
          margin-top: 4px;
        }

        .error-message {
          color: red;
          margin-bottom: 10px;
        }

        button {
          background-color: #007bff;
          color: #fff;
          padding: 10px;
          cursor: pointer;
        }

        button:hover {
          background-color: #0056b3;
        }

        p {
          text-align: center;
          margin-top: 10px;
        }
      `}</style>
    </div>
  );
};

export default Signup;
