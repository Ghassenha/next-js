// pages/login.js
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

const Login = () => {
  const router = useRouter();
  const [formData, setFormData] = useState({
    username: '',
    password: '',
    email: '', // Nouveau champ pour l'e-mail dans le formulaire de récupération de mot de passe
  });
  const [errorMessage, setErrorMessage] = useState('');

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleLoginSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: formData.username,
          password: formData.password,
          action: 'login',
        }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log('Received Token:', data.token);
        localStorage.setItem('token', data.token);
        router.push('/profil');
      } else {
        const errorData = await response.json();
        setErrorMessage(errorData.message);
      }
    } catch (error) {
      console.error('Unexpected Error during Login:', error);
    }
  };

  const handleForgotPasswordSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch('/api/reset-password', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: formData.email, // Utiliser l'e-mail pour la récupération de mot de passe
          action: 'forgot-password',
        }),
      });

      if (response.ok) {
        const data = await response.json();
        console.log(data.message);
        router.push('/reset-password-success');
      } else {
        const errorData = await response.json();
        setErrorMessage(errorData.message);
      }
    } catch (error) {
      console.error('Unexpected Error during Forgot Password:', error);
    }
  };

  return (
    <div className="login-container">
      <h2>Connexion</h2>
      {errorMessage && <p className="error-message">{errorMessage}</p>}
      <form onSubmit={handleLoginSubmit} className="login-form">
        <div className="form-group">
          <label>Email ou Numéro:</label>
          <input
            type="text"
            name="username"
            value={formData.username}
            onChange={handleInputChange}
          />
        </div>
        <div className="form-group">
          <label>Mot de passe:</label>
          <input
            type="password"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
          />
        </div>
        <button type="submit">Se connecter</button>
      </form>
      {/* Formulaire de récupération de mot de passe */}
      <form onSubmit={handleForgotPasswordSubmit} className="forgot-password-form">
        <div className="form-group">
          <label>Email:</label>
          <input
            type="email"
            name="email"
            value={formData.email}
            onChange={handleInputChange}
          />
        </div>
        <button type="submit">Réinitialiser le mot de passe</button>
      </form>
      <p className="signup-link">
        Vous n'avez pas de compte ?{' '}
        <Link href="/">Inscrivez-vous ici</Link>
      </p>
      <style jsx>{`
        .login-container {
          max-width: 400px;
          margin: auto;
          padding: 20px;
          border: 1px solid #ccc;
          border-radius: 8px;
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h2 {
          text-align: center;
          color: #333;
        }

        .login-form, .forgot-password-form {
          display: flex;
          flex-direction: column;
        }

        .form-group {
          margin-bottom: 10px;
        }

        label {
          margin-bottom: 5px;
        }

        input {
          padding: 8px;
          margin-top: 4px;
        }

        .error-message {
          color: red;
          margin-bottom: 10px;
        }

        button {
          background-color: #007bff;
          color: #fff;
          padding: 10px;
          cursor: pointer;
        }

        button:hover {
          background-color: #0056b3;
        }

        .signup-link {
          text-align: center;
          margin-top: 10px;
        }

        .forgot-password-link {
          text-decoration: underline;
          cursor: pointer;
        }
      `}</style>
    </div>
  );
};

export default Login;
