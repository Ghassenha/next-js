// pages/api/signup.js
import { query } from '../../db';
import bcrypt from 'bcrypt';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { nom, prenom, email, geolocalisation, statutManager, password, numero } = req.body;
    try {
      // Hachage du mot de passe
      const hashedPassword = await bcrypt.hash(password, 10);

      // Insertion dans la base de données
      const result = await query(
        'INSERT INTO utilisateurs (nom, prenom, email, geolocalisation, statut_manager, password, numero) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *',
        [nom, prenom, email, geolocalisation, statutManager, hashedPassword, numero]
      );

      // Réponse avec les données insérées
      res.status(200).json(result.rows[0]);
    } catch (error) {
      console.error('Error during signup:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  } else {
    res.status(405).json({ message: 'Method Not Allowed' });
  }
}
