// pages/api/reset-password.js
import { query } from '../../db';
import bcrypt from 'bcrypt';
import nodemailer from 'nodemailer';

const generateRandomPassword = (length) => {
  // Générer un mot de passe aléatoire avec la longueur spécifiée
  const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  let password = "";
  for (let i = 0; i < length; i++) {
    const randomIndex = Math.floor(Math.random() * charset.length);
    password += charset[randomIndex];
  }
  return password;
};

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { email } = req.body;

    try {
      // Vérifier si l'utilisateur avec cet e-mail existe
      const result = await query('SELECT * FROM utilisateurs WHERE email = $1', [email]);

      if (result.rows.length === 0) {
        res.status(404).json({ message: 'Utilisateur non trouvé' });
        return;
      }

      const user = result.rows[0];

      // Générer un nouveau mot de passe aléatoire
      const newPassword = generateRandomPassword(8);

      // Hacher le nouveau mot de passe
      const hashedPassword = await bcrypt.hash(newPassword, 10);

      // Mettre à jour le mot de passe de l'utilisateur dans la base de données
      await query('UPDATE utilisateurs SET password = $1 WHERE id = $2', [hashedPassword, user.id]);

      // Envoyer un e-mail avec le nouveau mot de passe
      const transporter = nodemailer.createTransport({
        // Configuration de votre serveur de messagerie (à remplacer)
        service: 'outlook',
        auth: {
          user: 'projetnext@outlook.com',
          pass: 'Azerty1234',
        },
      });

      const mailOptions = {
        from: 'projetnext@outlook.com',
        to: user.email,
        subject: 'Réinitialisation de mot de passe',
        text: `Votre nouveau mot de passe est : ${newPassword}`,
      };

      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          console.error('Error sending email:', error);
          res.status(500).json({ message: 'Internal server error' });
        } else {
          console.log('Email sent:', info.response);
          res.status(200).json({ message: 'Nouveau mot de passe envoyé par e-mail' });
        }
      });
    } catch (error) {
      console.error('Error during password reset:', error);
      res.status(500).json({ message: 'Internal server error' });
    }
  } else {
    res.status(405).json({ message: 'Method Not Allowed' });
  }
}
