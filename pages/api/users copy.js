// pages/api/users.js
import pool from "../../db"; // Assurez-vous d'ajuster le chemin correctement

export default async function handler(req, res) {
  try {
    if (req.method === "GET") {
      // Récupérer les données des utilisateurs
      const result = await pool.query("SELECT * FROM utilisateurs");
      const users = result.rows;
      res.status(200).json(users);
    } else if (req.method === "PUT") {
      // Modifier la colonne is_approved pour un utilisateur spécifique
      const { userId } = req.body;
      await pool.query(
        "UPDATE utilisateurs SET approuve = TRUE WHERE id = $1",
        [userId]
      );
      res.status(200).json({ message: "Utilisateur approuvé avec succès" });
    } else {
      res.status(405).json({ error: "Méthode non autorisée" });
    }
  } catch (error) {
    console.error("Erreur lors de la manipulation des utilisateurs:", error);
    res.status(500).json({ error: "Erreur interne du serveur" });
  }
}
