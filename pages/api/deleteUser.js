// pages/api/deleteUser.js

import pool from "../../db"; // Assurez-vous d'ajuster le chemin correctement

export default async function handler(req, res) {
  if (req.method === "DELETE") {
    const { userId } = req.body;

    try {
      const result = await pool.query(
        "DELETE FROM utilisateurs WHERE id = $1",
        [userId]
      );
      res.status(200).json({ message: "User deleted successfully" });
    } catch (error) {
      console.error("Error deleting user:", error);
      res.status(500).json({ message: "Internal Server Error" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }
}
