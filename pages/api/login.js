// pages/api/login.js
import { query } from '../../db';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import transporter from '../../mailer';

const JWT_SECRET = 'your_jwt_secret';

export default async function handler(req, res) {
  if (req.method === 'POST') {
    const { username, password, action } = req.body;

    if (action === 'login') {
      try {
        const result = await query(
          'SELECT * FROM utilisateurs WHERE email = $1 OR numero = $1',
          [username]
        );

        if (result.rows.length === 0) {
          res.status(401).json({ message: 'Utilisateur non trouvé' });
          return;
        }

        const user = result.rows[0];
        const match = await bcrypt.compare(password, user.password);

        if (!match) {
          res.status(401).json({ message: 'Mot de passe incorrect' });
          return;
        }

        const token = jwt.sign({ userId: user.id }, JWT_SECRET);
        res.status(200).json({ token });
      } catch (error) {
        console.error('Error during login:', error);
        res.status(500).json({ message: 'Internal server error' });
      }
    } else {
      res.status(400).json({ message: 'Action non valide' });
    }
  } else {
    res.status(405).json({ message: 'Method Not Allowed' });
  }
}
