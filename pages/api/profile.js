// pages/api/profile.js
import { query } from '../../db';
import { verifyToken } from '../../utils/auth';

export default async function handler(req, res) {
  const token = req.headers.authorization?.replace('Bearer ', '');

  try {
    if (!token) {
      console.log('Token manquant');
      res.status(401).json({ message: 'Unauthorized' });
      return;
    }

    // Vérifiez le token JWT
    const userId = verifyToken(token);

    if (!userId) {
      console.log('Token invalide');
      res.status(401).json({ message: 'Unauthorized' });
      return;
    }

    console.log('UserId:', userId);

    if (req.method === 'GET') {
      const result = await query('SELECT * FROM utilisateurs WHERE id = $1', [userId]);

      if (result.rows.length === 0) {
        console.log('Utilisateur non trouvé');
        res.status(404).json({ message: 'Utilisateur non trouvé' });
        return;
      }

      const user = result.rows[0];
      console.log('User found:', user);
      res.status(200).json(user);
    } else if (req.method === 'PUT') {
      const { nom, prenom, geolocalisation, numero } = req.body;
      const result = await query(
        'UPDATE utilisateurs SET nom = $1, prenom = $2, geolocalisation = $3, numero= $4 WHERE id = $5 RETURNING *',
        [nom, prenom, geolocalisation, numero, userId]
      );

      console.log('User updated:', result.rows[0]);
      res.status(200).json(result.rows[0]);
    } else {
      console.log('Méthode non autorisée');
      res.status(405).json({ message: 'Method Not Allowed' });
    }
  } catch (error) {
    console.error('Error in profile API:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
}
