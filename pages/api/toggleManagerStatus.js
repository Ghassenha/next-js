// pages/api/toggleManagerStatus.js

import pool from "../../db"; // Assurez-vous d'ajuster le chemin correctement

export default async function handler(req, res) {
  if (req.method === "PUT") {
    const { userId, isManager } = req.body;

    try {
      const result = await pool.query(
        "UPDATE utilisateurs SET statut_manager = $1 WHERE id = $2",
        [isManager, userId]
      );
      res.status(200).json({ message: "Manager status updated successfully" });
    } catch (error) {
      console.error("Error updating manager status:", error);
      res.status(500).json({ message: "Internal Server Error" });
    }
  } else {
    res.status(405).json({ message: "Method Not Allowed" });
  }
}
