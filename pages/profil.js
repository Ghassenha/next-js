import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

const Profil = () => {
  const router = useRouter();
  const [profileData, setProfileData] = useState({
    email: '',
    nom: '',
    prenom: '',
    numero: '',
    geolocalisation: '',
  });

  useEffect(() => {
    const fetchProfile = async () => {
      try {
        const token = localStorage.getItem('token');

        if (!token) {
          router.push('/login');
          return;
        }

        const response = await fetch('/api/profile', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        if (response.ok) {
          const data = await response.json();
          setProfileData(data);
        } else {
          const errorData = await response.json();
          console.error('Profile Fetch Error:', errorData);
          router.push('/login');
        }
      } catch (error) {
        console.error('Unexpected Error fetching Profile:', error);
      }
    };

    fetchProfile();
  }, []);

  const handleInputChange = (e) => {
    setProfileData({
      ...profileData,
      [e.target.name]: e.target.value,
    });
  };

  const handleUpdateProfile = async () => {
    try {
      const token = localStorage.getItem('token');
      const response = await fetch('/api/profile', {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(profileData),
      });

      if (response.ok) {
        const data = await response.json();
        setProfileData(data);
      } else {
        const errorData = await response.json();
        console.error('Profile Update Error:', errorData);
      }
    } catch (error) {
      console.error('Unexpected Error during Profile Update:', error);
    }
  };
  const handleLogout = () => {
    // Supprimer le token du localStorage
    localStorage.removeItem('token');
    // Rediriger vers la page de connexion
    router.push('/login');
  };

  return (
    <div className="profile-container">
      <h2>Profil Utilisateur</h2>
      <label>
        Nom:
        <input type="text" name="nom" value={profileData.nom} onChange={handleInputChange} />
      </label>
      <label>
        Prénom:
        <input type="text" name="prenom" value={profileData.prenom} onChange={handleInputChange} />
      </label>
      <label>
        Email:
        <input type="text" name="email" value={profileData.email} onChange={handleInputChange} />
      </label>
      <label>
        Numéro:
        <input type="text" name="numero" value={profileData.numero} onChange={handleInputChange} />
      </label>
      <label>
        Géolocalisation:
        <input
          type="text"
          name="geolocalisation"
          value={profileData.geolocalisation}
          onChange={handleInputChange}
        />
      </label>
      <button onClick={handleUpdateProfile}>Mettre à jour le profil</button>
      <button onClick={handleLogout}>Déconnexion</button>
      <style jsx>{`
        .profile-container {
          max-width: 400px;
          margin: auto;
          padding: 20px;
          border: 1px solid #ccc;
          border-radius: 8px;
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }

        h2 {
          text-align: center;
          color: #333;
        }

        label {
          display: block;
          margin-bottom: 10px;
        }

        input {
          width: 100%;
          padding: 8px;
          margin-top: 4px;
        }

        button {
          background-color: #007bff;
          color: #fff;
          padding: 10px;
          cursor: pointer;
          margin-top: 10px;
          width: 100%;
        }

        button:hover {
          background-color: #0056b3;
        }
      `}</style>
    </div>
  );
};

export default Profil;
