// Importez Link de next/link
import Link from 'next/link';
import { useState, useEffect } from 'react';

const AdminPage = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [users, setUsers] = useState([]);
  const [searchTerm, setSearchTerm] = useState(""); // État pour stocker la valeur de la recherche

  useEffect(() => {
    const isAuthenticated = localStorage.getItem("authenticated");
    if (isAuthenticated) {
      setAuthenticated(true);
      fetchUsers();
    }
  }, []);

  const authenticate = () => {
    if (username === process.env.ADMIN_USERNAME && password === process.env.ADMIN_PASSWORD) {
      setAuthenticated(true);
      localStorage.setItem("authenticated", "true");
      fetchUsers();
    } else {
      alert("Incorrect username or password");
    }
  };

  const logout = () => {
    // Effacez le token d'authentification lors de la déconnexion
    setAuthenticated(false);
    localStorage.removeItem("authenticated");
    setPassword(""); // Effacez le mot de passe
  };

  const fetchUsers = async () => {
    try {
      const response = await fetch("/api/users");
      if (response.ok) {
        const data = await response.json();
        setUsers(data);
      } else {
        console.error("Failed to fetch users");
      }
    } catch (error) {
      console.error("Unexpected error fetching users:", error);
    }
  };

  const handleApproveUser = async (userId) => {
    try {
      const response = await fetch("/api/users", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ userId }),
      });
      if (response.ok) {
        setUsers((prevUsers) => {
          return prevUsers.map((user) => {
            if (user.id === userId) {
              return { ...user, approuve: true };
            }
            return user;
          });
        });
      } else {
        console.error("Failed to approve user");
      }
    } catch (error) {
      console.error("Error approving user:", error);
    }
  };

  // Filtrer les utilisateurs en fonction de la recherche
  const filteredUsers = users.filter((user) =>
    `${user.nom} ${user.prenom}`
      .toLowerCase()
      .includes(searchTerm.toLowerCase())
  );

  if (!authenticated) {
    return (
      <div className="auth-container">
        <h1>Admin Authentication</h1>
        <input
          type="text"
          placeholder="Username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button onClick={authenticate}>Login</button>

        <style jsx>{`
          .auth-container {
            display: flex;
            max-width: 400px;
            margin: auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            flex-direction: column;
            align-items: center;
          }

          h1 {
            text-align: center;
            color: #333;
          }

          input {
            width: 100%;
            padding: 8px;
            margin-top: 10px;
          }

          button {
            width: 100%;
            background-color: #007bff;
            color: #fff;
            padding: 10px;
            cursor: pointer;
            margin-top: 10px;
          }

          button:hover {
            background-color: #0056b3;
          }
        `}</style>
      </div>
    );
  }

  return (
    <div className="admin-container">
      {/* Navigation à gauche */}
      <nav className="admin-nav">
        <ul>
          <li>
            <Link href="/admin">
              Demandes d'approbation
            </Link>
          </li>
          <li>
            <Link href="/admin2">
              Gestion des utilisateurs
            </Link>
          </li>
        </ul>
      </nav>

      {/* Contenu de la page */}
      <div className="admin-content">
        <h1>Demandes d'approbation</h1>
        <button onClick={logout}>Logout</button>
        <input
          type="text"
          placeholder="Search Users"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <h2>Liste des Utilisateurs</h2>
        <ul>
          {filteredUsers.map((user) => (
            <li key={user.id} className="user-item">
              {user.nom} {user.prenom} -{" "}
              {user.approuve ? "Approuvé" : "En attente"}
              {!user.approuve && (
                <button
                  onClick={() => {
                    handleApproveUser(user.id);
                  }}
                >
                  Approuver
                </button>
              )}
            </li>
          ))}
        </ul>
      </div>

      <style jsx>{`
        .admin-container {
          display: flex;
        }

        .admin-nav {
          flex: 0 0 200px;
          background-color: #f4f4f4;
          padding: 20px;
          box-shadow: 2px 0 5px rgba(0, 0, 0, 0.1);
        }

        .admin-nav ul {
          list-style: none;
          padding: 0;
        }

        .admin-nav li {
          margin-bottom: 10px;
        }

        .admin-nav a {
          text-decoration: none;
          color: #333;
          font-weight: bold;
        }

        .admin-nav a:hover {
          color: #007bff;
        }

        .admin-content {
          flex: 1;
          padding: 20px;
        }

        h1 {
          text-align: center;
          color: #333;
        }

        input {
          width: 100%;
          padding: 8px;
          margin-top: 10px;
        }

        ul {
          list-style: none;
          padding: 0;
        }

        .user-item {
          margin-bottom: 10px;
          padding: 10px;
          border: 1px solid #ccc;
          border-radius: 8px;
          box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
        }

        li button {
          margin-left: 10px;
          background-color: #28a745;
          color: #fff;
          padding: 5px 10px;
          cursor: pointer;
        }

        li button:hover {
          background-color: #218838;
        }
      `}</style>
    </div>
  );
};

export default AdminPage;
