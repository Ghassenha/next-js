// pages/reset-password-success.js
const ResetPasswordSuccessPage = () => (
    <div className="container">
      <h2>Mot de passe réinitialisé</h2>
      <p>Un nouveau mot de passe a été envoyé à votre adresse e-mail. Veuillez vérifier votre boîte de réception.</p>
      <style jsx>{`
        .container {
          max-width: 400px;
          margin: auto;
          padding: 20px;
          border: 1px solid #ccc;
          border-radius: 8px;
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
          text-align: center;
        }
  
        h2 {
          color: #333;
        }
  
        p {
          color: #666;
          margin-top: 20px;
        }
      `}</style>
    </div>
  );
  
  export default ResetPasswordSuccessPage;
  