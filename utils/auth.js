// utils/auth.js
import jwt from 'jsonwebtoken';

const JWT_SECRET = 'your_jwt_secret';

export const verifyToken = (token) => {
  try {
    const decoded = jwt.verify(token, JWT_SECRET);
    return decoded.userId;
  } catch (error) {
    console.error('Error verifying token:', error);
    return null;
  }
};
