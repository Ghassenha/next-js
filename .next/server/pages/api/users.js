"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/api/users";
exports.ids = ["pages/api/users"];
exports.modules = {

/***/ "next/dist/compiled/next-server/pages-api.runtime.dev.js":
/*!**************************************************************************!*\
  !*** external "next/dist/compiled/next-server/pages-api.runtime.dev.js" ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = require("next/dist/compiled/next-server/pages-api.runtime.dev.js");

/***/ }),

/***/ "pg":
/*!*********************!*\
  !*** external "pg" ***!
  \*********************/
/***/ ((module) => {

module.exports = require("pg");

/***/ }),

/***/ "(api)/./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fusers&preferredRegion=&absolutePagePath=.%2Fpages%5Capi%5Cusers.js&middlewareConfigBase64=e30%3D!":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fusers&preferredRegion=&absolutePagePath=.%2Fpages%5Capi%5Cusers.js&middlewareConfigBase64=e30%3D! ***!
  \****************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   config: () => (/* binding */ config),\n/* harmony export */   \"default\": () => (__WEBPACK_DEFAULT_EXPORT__),\n/* harmony export */   routeModule: () => (/* binding */ routeModule)\n/* harmony export */ });\n/* harmony import */ var next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/dist/server/future/route-modules/pages-api/module.compiled */ \"(api)/./node_modules/next/dist/server/future/route-modules/pages-api/module.compiled.js\");\n/* harmony import */ var next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_dist_server_future_route_kind__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/dist/server/future/route-kind */ \"(api)/./node_modules/next/dist/server/future/route-kind.js\");\n/* harmony import */ var next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/dist/build/templates/helpers */ \"(api)/./node_modules/next/dist/build/templates/helpers.js\");\n/* harmony import */ var _pages_api_users_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages\\api\\users.js */ \"(api)/./pages/api/users.js\");\n\n\n\n// Import the userland code.\n\n// Re-export the handler (should be the default export).\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_pages_api_users_js__WEBPACK_IMPORTED_MODULE_3__, \"default\"));\n// Re-export config.\nconst config = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_pages_api_users_js__WEBPACK_IMPORTED_MODULE_3__, \"config\");\n// Create and export the route module that will be consumed.\nconst routeModule = new next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0__.PagesAPIRouteModule({\n    definition: {\n        kind: next_dist_server_future_route_kind__WEBPACK_IMPORTED_MODULE_1__.RouteKind.PAGES_API,\n        page: \"/api/users\",\n        pathname: \"/api/users\",\n        // The following aren't used in production.\n        bundlePath: \"\",\n        filename: \"\"\n    },\n    userland: _pages_api_users_js__WEBPACK_IMPORTED_MODULE_3__\n});\n\n//# sourceMappingURL=pages-api.js.map//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9ub2RlX21vZHVsZXMvbmV4dC9kaXN0L2J1aWxkL3dlYnBhY2svbG9hZGVycy9uZXh0LXJvdXRlLWxvYWRlci9pbmRleC5qcz9raW5kPVBBR0VTX0FQSSZwYWdlPSUyRmFwaSUyRnVzZXJzJnByZWZlcnJlZFJlZ2lvbj0mYWJzb2x1dGVQYWdlUGF0aD0uJTJGcGFnZXMlNUNhcGklNUN1c2Vycy5qcyZtaWRkbGV3YXJlQ29uZmlnQmFzZTY0PWUzMCUzRCEiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFBc0c7QUFDdkM7QUFDTDtBQUMxRDtBQUNtRDtBQUNuRDtBQUNBLGlFQUFlLHdFQUFLLENBQUMsZ0RBQVEsWUFBWSxFQUFDO0FBQzFDO0FBQ08sZUFBZSx3RUFBSyxDQUFDLGdEQUFRO0FBQ3BDO0FBQ08sd0JBQXdCLGdIQUFtQjtBQUNsRDtBQUNBLGNBQWMseUVBQVM7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTCxZQUFZO0FBQ1osQ0FBQzs7QUFFRCIsInNvdXJjZXMiOlsid2VicGFjazovL215LW5leHRqcy1hcHAvP2U0MjIiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGFnZXNBUElSb3V0ZU1vZHVsZSB9IGZyb20gXCJuZXh0L2Rpc3Qvc2VydmVyL2Z1dHVyZS9yb3V0ZS1tb2R1bGVzL3BhZ2VzLWFwaS9tb2R1bGUuY29tcGlsZWRcIjtcbmltcG9ydCB7IFJvdXRlS2luZCB9IGZyb20gXCJuZXh0L2Rpc3Qvc2VydmVyL2Z1dHVyZS9yb3V0ZS1raW5kXCI7XG5pbXBvcnQgeyBob2lzdCB9IGZyb20gXCJuZXh0L2Rpc3QvYnVpbGQvdGVtcGxhdGVzL2hlbHBlcnNcIjtcbi8vIEltcG9ydCB0aGUgdXNlcmxhbmQgY29kZS5cbmltcG9ydCAqIGFzIHVzZXJsYW5kIGZyb20gXCIuL3BhZ2VzXFxcXGFwaVxcXFx1c2Vycy5qc1wiO1xuLy8gUmUtZXhwb3J0IHRoZSBoYW5kbGVyIChzaG91bGQgYmUgdGhlIGRlZmF1bHQgZXhwb3J0KS5cbmV4cG9ydCBkZWZhdWx0IGhvaXN0KHVzZXJsYW5kLCBcImRlZmF1bHRcIik7XG4vLyBSZS1leHBvcnQgY29uZmlnLlxuZXhwb3J0IGNvbnN0IGNvbmZpZyA9IGhvaXN0KHVzZXJsYW5kLCBcImNvbmZpZ1wiKTtcbi8vIENyZWF0ZSBhbmQgZXhwb3J0IHRoZSByb3V0ZSBtb2R1bGUgdGhhdCB3aWxsIGJlIGNvbnN1bWVkLlxuZXhwb3J0IGNvbnN0IHJvdXRlTW9kdWxlID0gbmV3IFBhZ2VzQVBJUm91dGVNb2R1bGUoe1xuICAgIGRlZmluaXRpb246IHtcbiAgICAgICAga2luZDogUm91dGVLaW5kLlBBR0VTX0FQSSxcbiAgICAgICAgcGFnZTogXCIvYXBpL3VzZXJzXCIsXG4gICAgICAgIHBhdGhuYW1lOiBcIi9hcGkvdXNlcnNcIixcbiAgICAgICAgLy8gVGhlIGZvbGxvd2luZyBhcmVuJ3QgdXNlZCBpbiBwcm9kdWN0aW9uLlxuICAgICAgICBidW5kbGVQYXRoOiBcIlwiLFxuICAgICAgICBmaWxlbmFtZTogXCJcIlxuICAgIH0sXG4gICAgdXNlcmxhbmRcbn0pO1xuXG4vLyMgc291cmNlTWFwcGluZ1VSTD1wYWdlcy1hcGkuanMubWFwIl0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///(api)/./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fusers&preferredRegion=&absolutePagePath=.%2Fpages%5Capi%5Cusers.js&middlewareConfigBase64=e30%3D!\n");

/***/ }),

/***/ "(api)/./db.js":
/*!***************!*\
  !*** ./db.js ***!
  \***************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

eval("// db.js\n\nconst { Pool } = __webpack_require__(/*! pg */ \"pg\");\nconst pool = new Pool({\n    user: process.env.DB_USER,\n    host: process.env.DB_HOST,\n    database: process.env.DB_NAME,\n    password: process.env.DB_PASSWORD,\n    port: process.env.DB_PORT\n});\nmodule.exports = {\n    query: (text, params)=>pool.query(text, params)\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9kYi5qcyIsIm1hcHBpbmdzIjoiQUFBQSxRQUFROztBQUVSLE1BQU0sRUFBRUEsSUFBSSxFQUFFLEdBQUdDLG1CQUFPQSxDQUFDO0FBRXpCLE1BQU1DLE9BQU8sSUFBSUYsS0FBSztJQUNwQkcsTUFBTUMsUUFBUUMsR0FBRyxDQUFDQyxPQUFPO0lBQ3pCQyxNQUFNSCxRQUFRQyxHQUFHLENBQUNHLE9BQU87SUFDekJDLFVBQVVMLFFBQVFDLEdBQUcsQ0FBQ0ssT0FBTztJQUM3QkMsVUFBVVAsUUFBUUMsR0FBRyxDQUFDTyxXQUFXO0lBQ2pDQyxNQUFNVCxRQUFRQyxHQUFHLENBQUNTLE9BQU87QUFDM0I7QUFFQUMsT0FBT0MsT0FBTyxHQUFHO0lBQ2ZDLE9BQU8sQ0FBQ0MsTUFBTUMsU0FBV2pCLEtBQUtlLEtBQUssQ0FBQ0MsTUFBTUM7QUFDNUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9teS1uZXh0anMtYXBwLy4vZGIuanM/NTY2NSJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBkYi5qc1xyXG5cclxuY29uc3QgeyBQb29sIH0gPSByZXF1aXJlKCdwZycpO1xyXG5cclxuY29uc3QgcG9vbCA9IG5ldyBQb29sKHtcclxuICB1c2VyOiBwcm9jZXNzLmVudi5EQl9VU0VSLFxyXG4gIGhvc3Q6IHByb2Nlc3MuZW52LkRCX0hPU1QsXHJcbiAgZGF0YWJhc2U6IHByb2Nlc3MuZW52LkRCX05BTUUsXHJcbiAgcGFzc3dvcmQ6IHByb2Nlc3MuZW52LkRCX1BBU1NXT1JELFxyXG4gIHBvcnQ6IHByb2Nlc3MuZW52LkRCX1BPUlQsXHJcbn0pO1xyXG5cclxubW9kdWxlLmV4cG9ydHMgPSB7XHJcbiAgcXVlcnk6ICh0ZXh0LCBwYXJhbXMpID0+IHBvb2wucXVlcnkodGV4dCwgcGFyYW1zKSxcclxufTtcclxuIl0sIm5hbWVzIjpbIlBvb2wiLCJyZXF1aXJlIiwicG9vbCIsInVzZXIiLCJwcm9jZXNzIiwiZW52IiwiREJfVVNFUiIsImhvc3QiLCJEQl9IT1NUIiwiZGF0YWJhc2UiLCJEQl9OQU1FIiwicGFzc3dvcmQiLCJEQl9QQVNTV09SRCIsInBvcnQiLCJEQl9QT1JUIiwibW9kdWxlIiwiZXhwb3J0cyIsInF1ZXJ5IiwidGV4dCIsInBhcmFtcyJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///(api)/./db.js\n");

/***/ }),

/***/ "(api)/./pages/api/users.js":
/*!****************************!*\
  !*** ./pages/api/users.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ handler)\n/* harmony export */ });\n/* harmony import */ var _db__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../db */ \"(api)/./db.js\");\n/* harmony import */ var _db__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_db__WEBPACK_IMPORTED_MODULE_0__);\n// pages/api/users.js\n // Assurez-vous d'ajuster le chemin correctement\nasync function handler(req, res) {\n    try {\n        if (req.method === \"GET\") {\n            // Récupérer les données des utilisateurs\n            const result = await _db__WEBPACK_IMPORTED_MODULE_0___default().query(\"SELECT * FROM utilisateurs\");\n            const users = result.rows;\n            res.status(200).json(users);\n        } else if (req.method === \"PUT\") {\n            // Modifier la colonne is_approved pour un utilisateur spécifique\n            const { userId } = req.body;\n            await _db__WEBPACK_IMPORTED_MODULE_0___default().query(\"UPDATE utilisateurs SET approuve = TRUE WHERE id = $1\", [\n                userId\n            ]);\n            res.status(200).json({\n                message: \"Utilisateur approuv\\xe9 avec succ\\xe8s\"\n            });\n        } else {\n            res.status(405).json({\n                error: \"M\\xe9thode non autoris\\xe9e\"\n            });\n        }\n    } catch (error) {\n        console.error(\"Erreur lors de la manipulation des utilisateurs:\", error);\n        res.status(500).json({\n            error: \"Erreur interne du serveur\"\n        });\n    }\n}\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9wYWdlcy9hcGkvdXNlcnMuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEscUJBQXFCO0FBQ08sQ0FBQyxnREFBZ0Q7QUFFOUQsZUFBZUMsUUFBUUMsR0FBRyxFQUFFQyxHQUFHO0lBQzVDLElBQUk7UUFDRixJQUFJRCxJQUFJRSxNQUFNLEtBQUssT0FBTztZQUN4Qix5Q0FBeUM7WUFDekMsTUFBTUMsU0FBUyxNQUFNTCxnREFBVSxDQUFDO1lBQ2hDLE1BQU1PLFFBQVFGLE9BQU9HLElBQUk7WUFDekJMLElBQUlNLE1BQU0sQ0FBQyxLQUFLQyxJQUFJLENBQUNIO1FBQ3ZCLE9BQU8sSUFBSUwsSUFBSUUsTUFBTSxLQUFLLE9BQU87WUFDL0IsaUVBQWlFO1lBQ2pFLE1BQU0sRUFBRU8sTUFBTSxFQUFFLEdBQUdULElBQUlVLElBQUk7WUFDM0IsTUFBTVosZ0RBQVUsQ0FDZCx5REFDQTtnQkFBQ1c7YUFBTztZQUVWUixJQUFJTSxNQUFNLENBQUMsS0FBS0MsSUFBSSxDQUFDO2dCQUFFRyxTQUFTO1lBQW1DO1FBQ3JFLE9BQU87WUFDTFYsSUFBSU0sTUFBTSxDQUFDLEtBQUtDLElBQUksQ0FBQztnQkFBRUksT0FBTztZQUF3QjtRQUN4RDtJQUNGLEVBQUUsT0FBT0EsT0FBTztRQUNkQyxRQUFRRCxLQUFLLENBQUMsb0RBQW9EQTtRQUNsRVgsSUFBSU0sTUFBTSxDQUFDLEtBQUtDLElBQUksQ0FBQztZQUFFSSxPQUFPO1FBQTRCO0lBQzVEO0FBQ0YiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9teS1uZXh0anMtYXBwLy4vcGFnZXMvYXBpL3VzZXJzLmpzPzQ5ZjYiXSwic291cmNlc0NvbnRlbnQiOlsiLy8gcGFnZXMvYXBpL3VzZXJzLmpzXHJcbmltcG9ydCBwb29sIGZyb20gXCIuLi8uLi9kYlwiOyAvLyBBc3N1cmV6LXZvdXMgZCdhanVzdGVyIGxlIGNoZW1pbiBjb3JyZWN0ZW1lbnRcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGFzeW5jIGZ1bmN0aW9uIGhhbmRsZXIocmVxLCByZXMpIHtcclxuICB0cnkge1xyXG4gICAgaWYgKHJlcS5tZXRob2QgPT09IFwiR0VUXCIpIHtcclxuICAgICAgLy8gUsOpY3Vww6lyZXIgbGVzIGRvbm7DqWVzIGRlcyB1dGlsaXNhdGV1cnNcclxuICAgICAgY29uc3QgcmVzdWx0ID0gYXdhaXQgcG9vbC5xdWVyeShcIlNFTEVDVCAqIEZST00gdXRpbGlzYXRldXJzXCIpO1xyXG4gICAgICBjb25zdCB1c2VycyA9IHJlc3VsdC5yb3dzO1xyXG4gICAgICByZXMuc3RhdHVzKDIwMCkuanNvbih1c2Vycyk7XHJcbiAgICB9IGVsc2UgaWYgKHJlcS5tZXRob2QgPT09IFwiUFVUXCIpIHtcclxuICAgICAgLy8gTW9kaWZpZXIgbGEgY29sb25uZSBpc19hcHByb3ZlZCBwb3VyIHVuIHV0aWxpc2F0ZXVyIHNww6ljaWZpcXVlXHJcbiAgICAgIGNvbnN0IHsgdXNlcklkIH0gPSByZXEuYm9keTtcclxuICAgICAgYXdhaXQgcG9vbC5xdWVyeShcclxuICAgICAgICBcIlVQREFURSB1dGlsaXNhdGV1cnMgU0VUIGFwcHJvdXZlID0gVFJVRSBXSEVSRSBpZCA9ICQxXCIsXHJcbiAgICAgICAgW3VzZXJJZF1cclxuICAgICAgKTtcclxuICAgICAgcmVzLnN0YXR1cygyMDApLmpzb24oeyBtZXNzYWdlOiBcIlV0aWxpc2F0ZXVyIGFwcHJvdXbDqSBhdmVjIHN1Y2PDqHNcIiB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJlcy5zdGF0dXMoNDA1KS5qc29uKHsgZXJyb3I6IFwiTcOpdGhvZGUgbm9uIGF1dG9yaXPDqWVcIiB9KTtcclxuICAgIH1cclxuICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgY29uc29sZS5lcnJvcihcIkVycmV1ciBsb3JzIGRlIGxhIG1hbmlwdWxhdGlvbiBkZXMgdXRpbGlzYXRldXJzOlwiLCBlcnJvcik7XHJcbiAgICByZXMuc3RhdHVzKDUwMCkuanNvbih7IGVycm9yOiBcIkVycmV1ciBpbnRlcm5lIGR1IHNlcnZldXJcIiB9KTtcclxuICB9XHJcbn1cclxuIl0sIm5hbWVzIjpbInBvb2wiLCJoYW5kbGVyIiwicmVxIiwicmVzIiwibWV0aG9kIiwicmVzdWx0IiwicXVlcnkiLCJ1c2VycyIsInJvd3MiLCJzdGF0dXMiLCJqc29uIiwidXNlcklkIiwiYm9keSIsIm1lc3NhZ2UiLCJlcnJvciIsImNvbnNvbGUiXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///(api)/./pages/api/users.js\n");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-api-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = __webpack_require__.X(0, ["vendor-chunks/next"], () => (__webpack_exec__("(api)/./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fusers&preferredRegion=&absolutePagePath=.%2Fpages%5Capi%5Cusers.js&middlewareConfigBase64=e30%3D!")));
module.exports = __webpack_exports__;

})();